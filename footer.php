<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="css/footer.css" rel="stylesheet" type="text/css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=0">
<!------ Include the above in your HEAD tag ---------->

<!-- Footer --><div class="borderlinetop"></div>
<section id="footer">
    <div class="footerimages">
        <img src="img/hout100.jpg" href="https://www.hout100procent.nl/" height="100px" width="100px">
        <img src="img/fscpng.png" href="http://www.fsc.nl/nl-nl" height="100px" width="100px">
        <img src="img/keurmerk-komo.png" href="https://www.komo.nl/" height="100px" width="120px">
        <img src="img/nbvtpng.png" href="https://nbvt.nl/" height="100px" width="150px">
        <img src="img/skhpng.png" href="https://www.skh.nl/nl/" height="100px" width="100px">
        <img src="img/veiligpng.png" href="https://www.politiekeurmerk.nl/" height="100px" width="100px">
        <img src="img/vriendenhoutpng.png" height="100px" width="150px">
    </div>
    <div class="borderline"></div>
    <div class="container-footer">

    <div class="col-xs-12 col-sm-4 col-md-4">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
                <ul class="list-unstyled list-inline social text-center">
                    <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-facebook"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-phone"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-linkedin"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void();" target="_blank"><i class="fa fa-envelope"></i></a></li>
                </ul>
            </div>
            </hr>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                <p class="h6">De Hoef &copy Gerealiseerd door<a class="text-green ml-2" href="https://www.2neighbours.nl" target="_blank">2Neighbours</a></p>
            </div>
            </hr>
        </div>
    </div>
</section>
<!-- ./Footer -->