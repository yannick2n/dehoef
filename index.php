<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>De Hoef</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/main.css" type="text/css">
    <link rel="stylesheet" href="css/navcss.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="script" href="js/bootstrap.html" type="text/html">
    <link rel="stylesheet" href="css/cards.css" type="text/css">
    <link rel="stylesheet" href="css/slider.css" type="text/css">
    <link rel="stylesheet" href="css/newscards.css" type="text/css">
    <link rel="stylesheet" href="css/indexcontact.css" type="text/css">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/js/bootstrap.js"></script>

    <!-- footer linkjes -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="css/footer.css" rel="stylesheet" type="text/css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://use.fontawesome.com/1744f3f671.js"></script>
</head>
<header>
    <!-- Navigation -->
    <div class="fixed-top">
        <header class="topbar">
            <div class="container">
                <div class="row">
                    <!-- social icon-->
                    <div class="col-sm-12">
                        <ul class="social-network">
                            <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-facebook"></i></a></li>
                          <!--  <li><a class="waves-effect waves-dark" href="#/"><i class="fa fa-instagram"></i></a></li> -->
                            <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a class="waves-effect waves-dark" href="tel:"><i class="fa fa-mobile-phone"></i></a></li>
                            <li><a class="waves-effect waves-dark" href="mailto:info@de-hoef.nl"><i class="fa fa-envelope"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </header>
        <nav class="navbar navbar-expand-lg navbar-light mx-background-top-linear">
            <div class="container">
                <img class="navbar-brand1" src="img/dehoefgoede.png">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">

                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="index.php">Home
                            </a>
                        </li>
                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="nav-link dropbtn">Producten</a>
                                <div class="dropdown-content">
                                    <a href="page/kozijnen.php">Kozijnen</a>
                                    <a href="page/ramen.php">Ramen</a>
                                    <a href="page/deuren.php">Deuren</a>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="page/project.php">Projecten</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="page/download.php">Downloads</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="page/vacatures.php">Vacatures</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="page/contact.php">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
    <!-- Cards -->
    <section class="details-card">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card-content">
                        <div class="card-img">
                            <img class="foto12" src="img/project3.jpg" alt="" width="572" height="358">
                            <span><h4>Projecten</h4></span>
                        </div>
                        <div class="card-desc">
                            <h3>Projecten</h3>
                            <p>Zie hier al onze projecten.
                               </p>
                            <a href="page/project.php" class="btn-card">Lees meer</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-content">
                        <div class="card-img">
                            <img class="foto12" src="img/kozijn.jpg" alt="" width="572" height="358">
                            <span><h4>Producten</h4></span>
                        </div>
                        <div class="card-desc">
                            <h3>Producten</h3>
                            <p>Zie hier alles wat wij maken.</p>
                            <a href="page/kozijnen.php" class="btn-card">Lees meer</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-content">
                        <div class="card-img">
                            <img class="foto12" src="img/fsc.jpg" alt="" >
                            <span><h4>Garanties</h4></span>
                        </div>
                        <div class="card-desc">
                            <h3>Garanties</h3>
                            <p>Zie hier de garanties die wij bieden</p>
                            <a href="#footer" class="btn-card">Zie meer</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end cards -->

</header>

<body>
<!-- text met slider -->
<div class="templateux-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 block-heading-wrap" data-aos="fade-up">
                <h2 class="heading mb-5 text-center">Houtindustrie de Hoef </h2>
            </div>
        </div> <!-- .row -->
        <div class="row mb-5" data-aos="fade-up">
            <div class="col-md-6">
                <p class="lead">Al meer dan 35 jaar produceert Houtindustrie De Hoef hoogwaardige kwaliteitskozijnen, ramen en deuren. Wij zijn een onderneming met zeer veel ervaring, ambacht en een hypermodern machinepark.

                    Wij staan bekend om onze persoonlijke aandacht en betrouwbare werkwijze. We zijn trots op de service die wij leveren. Het geeft klanten en opdrachtgevers van Houtindustrie de Hoef het vertrouwen dat zij mede met onze producten en adviezen het beste eindresultaat voor hun klanten kunnen realiseren. Onze jarenlange relatie met tevreden klanten is daarvan het beste bewijs.

                    Houtindustrie de Hoef levert haar producten onder KOMO-keur en HOUT 100%.</p>
            </div>
            <div class="col-md-6">
                <p>  <strong>Waarom Houtindustrie De Hoef ?</strong><br>
                    <strong>•</strong> Kwaliteit gebaseerd op meer dan 35 jaar kennis en ervaring.<br>
                    <strong>•</strong> Garanties van SGT en KOMO-keurmerk<br>
                    <strong>•</strong> Inbraakwering volgens politiekeurmerk Veilig Wonen.<br>
                    <strong>•</strong> Maatwerk voor zowel bedrijven als particulieren<br>
                    <strong>•</strong> Gebruik van verantwoord timmerhout (FSC™ ®, MTCC, PEFC).<br>
                    <strong>•</strong> Geen minimumgrootte voor orders<br>
                    <strong>•</strong> Moderne, deskundige en efficiënte bedrijfsvoering<br>
                    <strong>•</strong> Levering in 10 jaar garantie product zekerheid of 10 jaar+ prestatie zekerheid<br>
                    (zie voor meer informatie www.hout100procent.nl/zekerheden)</p>
            </div>
        </div>
    </div> <!-- .container -->
    <div class="container-fluid"  data-aos="fade-up">
        <div class="owl-carousel wide-slider">
            <div class="item">
                <img src="img/default.jpg" alt="" class="img-fluid">
            </div>
            <div class="item">
                <img src="img/default.jpg" alt="" class="img-fluid">
            </div>
            <div class="item">
                <img src="img/default.jpg" alt="" class="img-fluid">
            </div>
        </div> <!-- .owl-carousel -->
    </div>
</div> <!-- .templateux-section -->
<!-- end text met slider -->
<!-- newsCards -->
<h3 class="newsheader"><strong>Laatste projecten Houtindustrie De Hoef</strong></h3>
<section class="details-newscard">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img">
                        <img src="img/project2.jpg" alt="">
                        <span><h4>Project 1</h4></span>
                    </div>
                    <div class="card-desc">
                        <h3>Witte Bruggen Amersfoort</h3>
                        <p>Levering kozijnen voor 20 woningen Witte Bruggen te Amersfoort</p>
                        <a href="https://www.dehoef.2neighbours.nl/page/project.php#project1" class="btn-card">Zie meer</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img">
                        <img src="img/projecten/ede2.jpg" alt="">
                        <span><h4>Project 2</h4></span>
                    </div>
                    <div class="card-desc">
                        <h3>Park ter Does te Ede</h3>
                        <p>Levering kozijnen voor het werk 29 woningen Park ter Does te Ede</p>
                        <a href="https://www.dehoef.2neighbours.nl/page/project.php#project2" class="btn-card">Zie meer</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img">
                        <img src="img/projecten/leusden1.jpg" alt="">
                        <span><h4>Project 3</h4></span>
                    </div>
                    <div class="card-desc">
                        <h3>het huis van Leusden</h3>
                        <p>Levering kozijnen voor het werk ‘’het huis van Leusden’’ te Leusden</p>
                        <a href="https://www.dehoef.2neighbours.nl/page/project.php#project3" class="btn-card">Zie meer</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end newscards -->
<!-- contact section -->
<div class="section">
    <div class="top-border left"></div>
    <div class="top-border right"></div>
    <h1>CONTACT</h1>
    <p>Geintereseerd of vragen stuur ons dan een mail zodat wij u verder kunnen helpen!</p>
    <a href="page/contactformulier.php">CONTACT</a>
</div>
<!-- end contact section -->
</body>
<script src="js/slider/scripts-all.js"></script>
<script src="js/slider/main.js"></script>
<!-- Footer --><div class="borderlinetop"></div>
<section id="footer">
    <div class="footerimages">
        <a href="https://www.hout100procent.nl/" target="_blank"><img src="img/hout100.jpg" height="100px" width="100px">
            <a href="http://www.fsc.nl/nl-nl" target="_blank"><img src="img/fscpng.png" height="100px" width="100px">
                <a href="https://www.komo.nl/" target="_blank"><img src="img/keurmerk-komo.png" height="100px" width="120px">
                    <a href="https://nbvt.nl/" target="_blank"><img src="img/nbvtpng.png" height="100px" width="150px">
                        <a href="https://www.skh.nl/nl/" target="_blank"><img src="img/skhpng.png" height="100px" width="100px">
                            <a href="https://www.politiekeurmerk.nl/" target="_blank"><img src="img/veiligpng.png" height="100px" width="100px">
                                <a href="#" target="_blank"><img src="img/vriendenhoutpng.png" height="100px" width="150px">
    </div>
    <div class="borderline"></div>
    <div class="container-footer">

        <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
                    <ul class="list-unstyled list-inline social text-center">
                        <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-facebook"></i></a></li>
                        <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-phone"></i></a></li>
                        <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-linkedin"></i></a></li>
                        <li class="list-inline-item"><a href="javascript:void();" target="_blank"><i class="fa fa-envelope"></i></a></li>
                    </ul>
                </div>
                </hr>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                    <p class="h6">De Hoef &copy Gerealiseerd door<a class="text-green ml-2" href="https://www.2neighbours.nl" target="_blank">2Neighbours</a></p>
                </div>
                </hr>
            </div>
        </div>
</section>
<!-- ./Footer -->