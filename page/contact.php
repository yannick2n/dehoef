<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>De Hoef</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../css/main.css" type="text/css">
    <link rel="stylesheet" href="../css/navcss.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="script" href="../js/bootstrap.html" type="text/html">
    <link rel="stylesheet" href="../css/projectcss.css" type="text/css">

    <!-- footer linkjes -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="../css/footer.css" rel="stylesheet" type="text/css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="../css/contact.css" type="text/css">

</head>
<header>
    <div class="fixed-top">
        <header class="topbar">
            <div class="container">
                <div class="row">
                    <!-- social icon-->
                    <div class="col-sm-12">
                        <ul class="social-network">
                            <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-facebook"></i></a></li>
                            <!--  <li><a class="waves-effect waves-dark" href="https://www.instagram.com/2neighbours/"><i class="fa fa-instagram"></i></a></li> -->
                            <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a class="waves-effect waves-dark" href="tel:"><i class="fa fa-mobile-phone"></i></a></li>
                            <li><a class="waves-effect waves-dark" href="mailto:info@de-hoef.nl"><i class="fa fa-envelope"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </header>
        <nav class="navbar navbar-expand-lg navbar-light mx-background-top-linear">
            <div class="container">
                <img class="navbar-brand" src="../img/dehoefgoede.png">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">

                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="../index.php">Home
                            </a>
                        </li>
                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="nav-link dropbtn">Producten</a>
                                <div class="dropdown-content">
                                    <a href="kozijnen.php">Kozijnen</a>
                                    <a href="deuren.php">Deuren</a>
                                    <a href="ramen.php">Ramen</a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="project.php">Projecten</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="download.php">Downloads</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="vacatures.php">Vacatures</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="contact.php">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!-- page-header -->
    <div class="page-header" id="home">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-caption">
                        <h1 class="page-title"><br></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page-header-->
</header>

<body>
<div class="container-fluid">
    <div class="row" style="margin-top:20px;">
        <div class="leftcontainer">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-7">
                            <p class="card-text-contact" style="font-weight: 700;color:#404041;line-height:30px;font-size: 20px;">
                                    Contact

                            </p>
                        </div>
                        <div class="col-md-7">
                            <p class="card-text-text" style="font-size: 13px;line-height: 24px;color: #58595b;font-weight: 200;">
                                Heeft u vragen of opmerkingen neem dan contact met ons op!
                                <div>
                                <a href="contactformulier.php" class="btn btn-blog contactbtn">Contact</a>
                            </div>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="rightcontainer">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-7">
                            <p class="card-text-contact" style="font-weight: 700;color:#404041;line-height:30px;font-size: 20px;">
                                    Klachten

                            </p>
                        </div>
                        <div class="col-md-7">
                            <p class="card-text-text" style="font-size: 13px;line-height: 24px;color: #58595b;font-weight: 200;">
                                Heeft u klachten of opmerkingen laat het ons weten!
                            <div>
                                <a href="klachtenformulier.php" class="btn btn-blog contactbtn">Klachten</a>
                            </div>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="leftcontainer">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-7">
                                    <p class="card-text-contact" style="font-weight: 700;color:#404041;line-height:30px;font-size: 20px;">
                                            Bij ons werken

                                    </p>
                                </div>
                                <div class="col-md-7">
                                    <p class="card-text-text" style="font-size: 13px;line-height: 24px;color: #58595b;font-weight: 200;">
                                        Heeft u vragen of opmerkingen neem dan contact met ons op!
                                    <div>
                                        <a href="vacatures.php" class="btn btn-blog contactbtn">Vacatures</a>
                                    </div>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="rightcontainer">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-7">
                                    <p class="card-text-contact" style="font-weight: 700;color:#404041;line-height:30px;font-size: 20px;">
                                            Downloads

                                    </p>
                                </div>
                                <div class="col-md-7">
                                    <p class="card-text-text" style="font-size: 13px;line-height: 24px;color: #58595b;font-weight: 200;">
                                        Op deze pagina staat ondersteunend materiaal
                                    <div>
                                        <a href="download.php" class="btn btn-blog contactbtn">Downloads</a>
                                    </div>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="width: 100%"><iframe width="100%" height="600" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=Computerweg%2018%2C%20amersfoort+(De-hoef)&amp;ie=UTF8&amp;t=&amp;z=18&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Embed Google Map</a></iframe></div><br />

</body>


<!-- Footer --><div class="borderlinetop"></div>
<section id="footer">
    <div class="footerimages">
        <a href="https://www.hout100procent.nl/" target="_blank"><img src="../img/hout100.jpg" height="100px" width="100px">
            <a href="http://www.fsc.nl/nl-nl" target="_blank"><img src="../img/fscpng.png" height="100px" width="100px">
                <a href="https://www.komo.nl/" target="_blank"><img src="../img/keurmerk-komo.png" height="100px" width="120px">
                    <a href="https://nbvt.nl/" target="_blank"><img src="../img/nbvtpng.png" height="100px" width="150px">
                        <a href="https://www.skh.nl/nl/" target="_blank"><img src="../img/skhpng.png" height="100px" width="100px">
                            <a href="https://www.politiekeurmerk.nl/" target="_blank"><img src="../img/veiligpng.png" height="100px" width="100px">
                                <a href="#" target="_blank"><img src="../img/vriendenhoutpng.png" height="100px" width="150px">
    </div>
    <div class="borderline"></div>
    <div class="container-footer">

        <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
                    <ul class="list-unstyled list-inline social text-center">
                        <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-facebook"></i></a></li>
                        <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-phone"></i></a></li>
                        <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-linkedin"></i></a></li>
                        <li class="list-inline-item"><a href="javascript:void();" target="_blank"><i class="fa fa-envelope"></i></a></li>
                    </ul>
                </div>
                </hr>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                    <p class="h6">De Hoef &copy Gerealiseerd door<a class="text-green ml-2" href="https://www.2neighbours.nl" target="_blank">2Neighbours</a></p>
                </div>
                </hr>
            </div>
        </div>
</section>
<!-- ./Footer -->