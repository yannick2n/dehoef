<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>De Hoef</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../css/main.css" type="text/css">
    <link rel="stylesheet" href="../css/navcss.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="script" href="../js/bootstrap.html" type="text/html">
    <link rel="stylesheet" href="../css/cards.css" type="text/css">
    <link rel="stylesheet" href="../css/projectcss.css" type="text/css">
    <!-- footer linkjes -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="../css/footer.css" rel="stylesheet" type="text/css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<header>
    <div class="fixed-top">
        <header class="topbar">
            <div class="container">
                <div class="row">
                    <!-- social icon-->
                    <div class="col-sm-12">
                        <ul class="social-network">
                            <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-facebook"></i></a></li>
                            <!--  <li><a class="waves-effect waves-dark" href="https://www.instagram.com/2neighbours/"><i class="fa fa-instagram"></i></a></li> -->
                            <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a class="waves-effect waves-dark" href="tel:"><i class="fa fa-mobile-phone"></i></a></li>
                            <li><a class="waves-effect waves-dark" href="mailto:info@de-hoef.nl"><i class="fa fa-envelope"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </header>
        <nav class="navbar navbar-expand-lg navbar-light mx-background-top-linear">
            <div class="container">
                <img class="navbar-brand" src="../img/dehoefgoede.png">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">

                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="../index.php">Home
                            </a>
                        </li>
                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="nav-link dropbtn">Producten</a>
                                <div class="dropdown-content">
                                    <a href="kozijnen.php">Kozijnen</a>
                                    <a href="deuren.php">Deuren</a>
                                    <a href="ramen.php">Ramen</a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="project.php">Projecten</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="download.php">Downloads</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="vacatures.php">Vacatures</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link dropbtn" href="contact.php">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!-- page-header -->
    <div class="page-header" id="home">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-caption">
                        <h1 class="page-title"><br></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page-header-->
</header>

<body>
<!-- start project cards -->
<div id="project1"></div>
<div class="col-md-10 blogShort">
    <h2 class="hoofd">Witte Bruggen Amersfoort</h2>
    <img src="../img/project2.jpg" alt="" class="pull-left img-responsive thumb margin10 img-thumbnail fotoproject">

    <article><p class="tekst">
            Levering kozijnen voor 20 woningen Witte Bruggen te Amersfoort
        </p></article>
    <img class="btn btn-bloguit btngoed margin10">
</div>
<div id="project2"></div>
<div class="col-md-10 blogShort">
    <h2 class="hoofd">Park ter Does te Ede</h2>
    <img src="../img/projecten/ede1.jpg" alt="" class="pull-left img-responsive thumb margin10 img-thumbnail fotoproject">
    <article><p class="tekst">
            Levering kozijnen voor het werk 29 woningen Park ter Does te Ede
        </p></article>
    <img class="btn btn-bloguit btngoed margin10">
</div>
<div id="project3"></div>
<div class="col-md-10 blogShort">
    <h2 class="hoofd">het huis van Leusden</h2>
    <img src="../img/projecten/leusden1.jpg" alt="" class="pull-left img-responsive thumb margin10 img-thumbnail fotoproject">
    <article><p class="tekst">
            Levering kozijnen voor het werk ‘’het huis van Leusden’’ te Leusden
        </p></article>
    <img class="btn btn-bloguit btngoed margin10">
</div>
<div id="project4"></div>
<div class="col-md-10 blogShort">
    <h2 class="hoofd">Molenbeek te Nunspeet</h2>
    <img src="../img/projecten/nunspeet1.jpg" alt="" class="pull-left img-responsive thumb margin10 img-thumbnail fotoproject">
    <article><p class="tekst">
            Levering kozijnen voor het werk 36 + 37 woningen Molenbeek te Nunspeet
        </p></article>
    <img class="btn btn-bloguit btngoed margin10">
</div>
<div id="project4"></div>
<div class="col-md-10 blogShort">
    <h2 class="hoofd">Rijsenburgsehof te Driebergen</h2>
    <img src="../img/projecten/driebergen1.jpg" alt="" class="pull-left img-responsive thumb margin10 img-thumbnail fotoproject">
    <article><p class="tekst">
            Levering kozijnen voor het werk 23 woningen Rijsenburgsehof te Driebergen
        </p></article>
    <img class="btn btn-bloguit btngoed margin10">
</div>
<div id="project4"></div>
<div class="col-md-10 blogShort">
    <h2 class="hoofd">Dreamz te Houten</h2>
    <img src="../img/projecten/houten.jpg" alt="" class="pull-left img-responsive thumb margin10 img-thumbnail fotoproject">
    <article><p class="tekst">
            Levering kozijnen voor het werk 24 appartementen Dreamz te Houten
        </p></article>
    <img class="btn btn-bloguit btngoed margin10">
</div>
<div id="project4"></div>
<div class="col-md-10 blogShort">
    <h2 class="hoofd">De Hinde te Ermelo</h2>
    <img src="../img/projecten/ermelo1.jpg" alt="" class="pull-left img-responsive thumb margin10 img-thumbnail fotoproject">
    <article><p class="tekst">
            Levering kozijnen voor het werk 14 appartementen De Hinde te Ermelo
        </p></article>
    <img class="btn btn-bloguit btngoed margin10">
</div>
<div id="project4"></div>
<div class="col-md-10 blogShort">
    <h2 class="hoofd">Parkkwartier te Zeewolde</h2>
    <img src="../img/projecten/zeewolde.jpg" alt="" class="pull-left img-responsive thumb margin10 img-thumbnail fotoproject">
    <article><p class="tekst">
            Levering kozijnen voor het werk 7+4 woningen Parkkwartier te Zeewolde
        </p></article>
    <img class="btn btn-bloguit btngoed margin10">
</div>
<div id="project4"></div>
<div class="col-md-10 blogShort">
    <h2 class="hoofd">Meidoornlaan te Putten</h2>
    <img src="../img/projecten/meidoorputten2.jpg" alt="" class="pull-left img-responsive thumb margin10 img-thumbnail fotoproject">
    <article><p class="tekst">
            Levering kozijnen voor het werk 2 woningen Meidoornlaan te Putten
        </p></article>
    <img class="btn btn-bloguit btngoed margin10">
</div>
<div id="project4"></div>
<div class="col-md-10 blogShort">
    <h2 class="hoofd">Brinkstraat te Puttenn</h2>
    <img src="../img/projecten/brinkputten1.jpg" alt="" class="pull-left img-responsive thumb margin10 img-thumbnail fotoproject">
    <article><p class="tekst">
            Levering kozijnen voor het werk 13+13+26 senioren woningen Brinkstraat te Putten
        </p></article>
    <img class="btn btn-bloguit btngoed margin10">
</div>
<div id="project4"></div>
<div class="col-md-10 blogShort">
    <h2 class="hoofd">Bijsteren te Putten</h2>
    <img src="../img/projecten/bijsterenputten1.jpg" alt="" class="pull-left img-responsive thumb margin10 img-thumbnail fotoproject">
    <article><p class="tekst">
            Levering kozijnen voor het werk 4 twee kappers Bijsteren te Putten
        </p></article>
    <img class="btn btn-bloguit btngoed margin10">
</div>
<div id="project4"></div>
<div class="col-md-10 blogShort">
    <h2 class="hoofd">Lupinehof te Alphen aan de rijn</h2>
    <img src="../img/projecten/rhijn1.jpg" alt="" class="pull-left img-responsive thumb margin10 img-thumbnail fotoproject">
    <article><p class="tekst">
            Levering kozijnen voor het werk Lupinehof te Alphen aan de rijn
        </p></article>
    <img class="btn btn-bloguit btngoed margin10">
</div>
<div id="project4"></div>
<div class="col-md-10 blogShort">
    <h2 class="hoofd">woningen te Hoevelaken</h2>
    <img src="../img/projecten/hoevelaken1.jpg" alt="" class="pull-left img-responsive thumb margin10 img-thumbnail fotoproject">
    <article><p class="tekst">
            Levering kozijnen voor het werk Globe terrein, 7 + 4 woningen te Hoevelaken
        </p></article>
    <img class="btn btn-bloguit btngoed margin10">
</div>

<div class="col-md-12 gap10"></div>
</div>
<!-- end project cards -->
</body>

<!-- Footer --><div class="borderlinetop"></div>
<section id="footer">
    <div class="footerimages">
        <a href="https://www.hout100procent.nl/" target="_blank"><img src="../img/hout100.jpg" height="100px" width="100px">
            <a href="http://www.fsc.nl/nl-nl" target="_blank"><img src="../img/fscpng.png" height="100px" width="100px">
                <a href="https://www.komo.nl/" target="_blank"><img src="../img/keurmerk-komo.png" height="100px" width="120px">
                    <a href="https://nbvt.nl/" target="_blank"><img src="../img/nbvtpng.png" height="100px" width="150px">
                        <a href="https://www.skh.nl/nl/" target="_blank"><img src="../img/skhpng.png" height="100px" width="100px">
                            <a href="https://www.politiekeurmerk.nl/" target="_blank"><img src="../img/veiligpng.png" height="100px" width="100px">
                                <a href="#" target="_blank"><img src="../img/vriendenhoutpng.png" height="100px" width="150px">
    </div>
    <div class="borderline"></div>
    <div class="container-footer">

        <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
                    <ul class="list-unstyled list-inline social text-center">
                        <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-facebook"></i></a></li>
                        <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-phone"></i></a></li>
                        <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-linkedin"></i></a></li>
                        <li class="list-inline-item"><a href="javascript:void();" target="_blank"><i class="fa fa-envelope"></i></a></li>
                    </ul>
                </div>
                </hr>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                    <p class="h6">De Hoef &copy Gerealiseerd door<a class="text-green ml-2" href="https://www.2neighbours.nl" target="_blank">2Neighbours</a></p>
                </div>
                </hr>
            </div>
        </div>
</section>
<!-- ./Footer -->